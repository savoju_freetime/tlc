#TLC

Nextion code will be added soon to the project folders.

Touch screen light controller with STM32F030F4Px:
- 4 PWM channels
- RS232 serial
- 2 x indicator LEDs (Green and Red)
- Programming connection (Tag connect)
- Reserve for Encoder

A lot of more features under development.

Schematic, layout and source code is free to use.

NOTE! Notice this PCB and Code is made on my freetime and does not hold any kind approved tests.