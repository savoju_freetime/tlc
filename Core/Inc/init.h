/*----------------------------------------------------
 * Name:        init.h
 * Purpose:     
 *
 *----------------------------------------------------
 * Project:     X project
 *----------------------------------------------------
 * Compiler:    
 *----------------------------------------------------
 * Copyright:
 *----------------------------------------------------
 * Created:     27.07.2018 by savojuus
 *----------------------------------------------------
 * Globals:
 *----------------------------------------------------
 * $Rev: 0000 $:
 * $Author: xxxxxx $:
 * $Date: 2000-01-01 00:00:00 +0300 (xx, 01 tammi 2000) $:
 *----------------------------------------------------
 */
#ifndef __INIT_H__
#define __INIT_H__

/*
 * Include files
 */


/*
 * Local macro definitions
 */
 
 
/*
 * Type definitions
 */


/*
 * Local variables
 */

 
/* 
 * Function prototypes 
 */
void initialize_all(void);

#endif /* __INIT_H__ */
/* The end of file */
