/*----------------------------------------------------
 * Name:        filter.h
 * Purpose:     
 *
 *----------------------------------------------------
 * Project:     X project
 *----------------------------------------------------
 * Compiler:    
 *----------------------------------------------------
 * Copyright:
 *----------------------------------------------------
 * Created:     27.07.2018 by savojuus
 *----------------------------------------------------
 * Globals:
 *----------------------------------------------------
 * $Rev: 0000 $:
 * $Author: xxxxxx $:
 * $Date: 2000-01-01 00:00:00 +0300 (xx, 01 tammi 2000) $:
 *----------------------------------------------------
 */
#ifndef __FILTER_H__
#define __FILTER_H__

/*
 * Include files
 */
#include <stdio.h>

/*
 * Local macro definitions
 */
#define NBR_OF_PULSES_IN    2
#define NBR_OF_BUTTONS      1

#define UP_TIME_BUTTON      500
#define DOWN_TIME_BUTTON    500

#define UP_TIME_PULSE       1
#define DOWN_TIME_PULSE     1

#define TRANSITION_HIGH     1
#define TRANSITION_LOW      -1
#define NO_TRANSITION       0

#define BUTTON              0
#define ENCODER_A           1
#define ENCODER_B           2
 
/*
 * Type definitions
 */
typedef struct
{
    uint8_t encoderA;
    uint8_t encoderB;
    uint8_t enc_button;
}ENCODER_T;

typedef struct
{
    uint32_t ticks;
    uint32_t last_ticks;
    uint32_t up_time;
    uint32_t down_time;
    int8_t state;
    int8_t last_state;
}FILTER_T;
/*
 * Local variables
 */

 
/* 
 * Function prototypes 
 */
void init_filter(void);
void filter_io(void);

#endif /* __FILTER_H__ */
/* The end of file */
