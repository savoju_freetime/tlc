/*----------------------------------------------------
 * Name:        pwm.h
 * Purpose:     
 *
 *----------------------------------------------------
 * Project:     X project
 *----------------------------------------------------
 * Compiler:    
 *----------------------------------------------------
 * Copyright:
 *----------------------------------------------------
 * Created:     27.07.2018 by savojuus
 *----------------------------------------------------
 * Globals:
 *----------------------------------------------------
 * $Rev: 0000 $:
 * $Author: xxxxxx $:
 * $Date: 2000-01-01 00:00:00 +0300 (xx, 01 tammi 2000) $:
 *----------------------------------------------------
 */
#ifndef __PWM_H__
#define __PWM_H__

/*
 * Include files
 */
#include <stdio.h>

/*
 * Local macro definitions
 */
 
 
/*
 * Type definitions
 */
#define MAX_MSG_LEN		10

typedef struct{
    char rx[2];
    uint8_t end_char;
    uint8_t rx_complete;
    uint8_t message_complete;
    uint8_t buffer[8];
} serial_receive_t;

typedef struct{
#define ID_MEM_SLOT     4
#define ID_RED          3
#define ID_GREEN        4
#define ID_BLUE         5
#define ID_WHITE        6
#define ID_LESS			7
#define ID_OFF			8
#define ID_MORE			9
#define ID_SPECTRUM		10
#define ID_PARTY		11

    uint8_t new_redvalue;
    uint8_t new_greenvalue;
    uint8_t new_bluevalue;
    uint8_t new_whitevalue;

    uint16_t red_value;
    uint16_t green_value;
    uint16_t blue_value;
    uint16_t white_value;

    uint8_t buffer[8];
} pwm_handler_t;

/*
 * Local variables
 */

 
/* 
 * Function prototypes 
 */
void init_pwm(void);
void handle_pwm(void);
void get_value(void);

void nextion_msg(uint8_t *msg);

#endif /* __PWM_H__ */
/* The end of file */
