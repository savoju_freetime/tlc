/*----------------------------------------------------
 * Name:        leds.h
 * Purpose:     
 *
 *----------------------------------------------------
 * Project:     X project
 *----------------------------------------------------
 * Compiler:    
 *----------------------------------------------------
 * Copyright:
 *----------------------------------------------------
 * Created:     27.07.2018 by savojuus
 *----------------------------------------------------
 * Globals:
 *----------------------------------------------------
 * $Rev: 0000 $:
 * $Author: xxxxxx $:
 * $Date: 2000-01-01 00:00:00 +0300 (xx, 01 tammi 2000) $:
 *----------------------------------------------------
 */
#ifndef __LEDS_H__
#define __LEDS_H__

/*
 * Include files
 */

/*
 * Local macro definitions
 */
 
 
/*
 * Type definitions
 */
#define BLINK100    100
#define BLINK250    250
#define BLINK500    500
#define BLINK1000   1000



/*
 * Local variables
 */

 
/* 
 * Function prototypes 
 */
void init_leds(void);
void handle_leds(void);

#endif /* __LEDS_H__ */
/* The end of file */
