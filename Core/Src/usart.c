/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */
#include "pwm.h"
#include <stdint.h>
#include <string.h>

extern serial_receive_t serial;
extern pwm_handler_t pwm_handler;

uint8_t rx_data[2];
uint8_t rx_buffer[10];
/* USER CODE END 0 */

UART_HandleTypeDef huart1;

/* USART1 init function */

void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspInit 0 */

  /* USER CODE END USART1_MspInit 0 */
    /* USART1 clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();
  
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART1 GPIO Configuration    
    PA2     ------> USART1_TX
    PA3     ------> USART1_RX 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF1_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspInit 1 */

  /* USER CODE END USART1_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspDeInit 0 */

  /* USER CODE END USART1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART1_CLK_DISABLE();
  
    /**USART1 GPIO Configuration    
    PA2     ------> USART1_TX
    PA3     ------> USART1_RX 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2|GPIO_PIN_3);

    /* USART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspDeInit 1 */

  /* USER CODE END USART1_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
void start_uart(void)
{
    HAL_UART_Receive_IT(&huart1, rx_data, 1);
    memset(&rx_buffer, 0x00, 10);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    static uint8_t i = 0; /* idx */

    if ( huart->Instance == USART1 ) /* Usart 1 receiving? */
    {
        rx_buffer[i] = rx_data[0];

        if ( rx_buffer[i] == 0xFF ) /* End character is received */
        {
            serial.end_char++; /* Observe end characters, might be end of message */
            i++;
        }
        else
        {
            serial.end_char = 0; /* Reset end character observation */
            i++;
        }

        if ( i >= 10 ) /* if message is too long reset buffer */
        {
            serial.end_char = 0;
            i = 0;
            memset(&rx_buffer, 0x00, 10);
        }

        if ( serial.end_char == 3 ) /* All end characters received, copy msg, reset buffer and perform actions */
        {
            serial.end_char = 0;
            i = 0;
//            memcpy(&r_msg_nextion, &rx_buffer, sizeof(r_msg_nextion));

            nextion_msg(&rx_buffer[0]);
#if 0

            if ( rx_buffer[ID_MEM_SLOT] == ID_RED )
            {
                pwm_handler.red_value = (uint16_t)rx_buffer[1] << 8;
                pwm_handler.red_value |= rx_buffer[0];
                pwm_handler.new_redvalue = 1;
            }

            if ( rx_buffer[ID_MEM_SLOT] == ID_GREEN )
            {
                pwm_handler.green_value = (uint16_t)rx_buffer[1] << 8;
                pwm_handler.green_value |= rx_buffer[0];
                pwm_handler.new_greenvalue = 1;
            }

            if ( rx_buffer[ID_MEM_SLOT] == ID_BLUE )
            {
                pwm_handler.blue_value = (uint16_t)rx_buffer[1] << 8;
                pwm_handler.blue_value |= rx_buffer[0];
                pwm_handler.new_bluevalue = 1;
            }

            if ( rx_buffer[ID_MEM_SLOT] == ID_WHITE )
            {
                pwm_handler.white_value = (uint16_t)rx_buffer[1] << 8;
                pwm_handler.white_value |= rx_buffer[0];
                pwm_handler.new_whitevalue = 1;
            }
#endif
            memset(&rx_buffer, 0x00, 10);
        }


        HAL_UART_Receive_IT(&huart1, rx_data, 1);
    }
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
