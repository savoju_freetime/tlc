/*----------------------------------------------------
 * Name:        pwm.c
 * Purpose:     
 *
 *----------------------------------------------------
 * Project:     X project
 *----------------------------------------------------
 * Compiler:    
 *----------------------------------------------------
 * Copyright:
 *----------------------------------------------------
 * Created:     27.07.2018 by savojuus
 *----------------------------------------------------
 * Globals:
 *----------------------------------------------------
 * $Rev: 0000 $:
 * $Author: xxxxxx $:
 * $Date: 2000-01-01 00:00:00 +0300 (xx, 01 tammi 2000) $:
 *----------------------------------------------------
 */


/*
 * Include files
 */
#include "pwm.h"
#include "tim.h"
#include <stdbool.h>
#include <string.h>

/*
 * Local macro definitions
 */
 
 
/*
 * Type definitions
 */


/*
 * Local variables
 */
serial_receive_t serial = {0};
pwm_handler_t pwm_handler = {0};
uint8_t r_msg_nextion[MAX_MSG_LEN] = {0}; /* received message from nextion module */
bool spectrum_toggle = false;
bool party_toggle = false;

/* 
 * Function prototypes 
 */
static void red_pwm(void);
static void green_pwm(void);
static void blue_pwm(void);
static void white_pwm(void);
static void spectrum_pwm(void);
static void party_pwm(void);

/* 
 * Functions
 */
void init_pwm(void)
{
    HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3); /* WHITE */
    HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2); /* RED */
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4); /* BLUE */
    HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1); /* GREEN */

    __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, 0); /* WHITE */
    __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, 0); /* RED */
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, 0); /* BLUE */
    __HAL_TIM_SET_COMPARE(&htim14, TIM_CHANNEL_1, 0); /* GREEN */
}

void get_value(void)
{
}

void nextion_msg(uint8_t* msg)
{
	memcpy(&r_msg_nextion, &msg, sizeof(r_msg_nextion));
}

void handle_pwm(void)
{

	switch(r_msg_nextion[ID_MEM_SLOT])
	{
		case ID_RED:
			red_pwm();
			break;

		case ID_GREEN:
			green_pwm();
			break;

		case ID_BLUE:
			blue_pwm();
			break;

		case ID_WHITE:
			white_pwm();
			break;

		case ID_LESS:
			white_pwm();
			break;

		case ID_OFF:
			white_pwm();
			break;

		case ID_MORE:
			white_pwm();
			break;

		case ID_SPECTRUM:
			spectrum_toggle = (spectrum_toggle == false ? true : false);
			break;

		case ID_PARTY:
			break;

		default:
			/* Do nothing? */
			break;
	}

	memset(&r_msg_nextion, 0x00, sizeof(r_msg_nextion)); /* Reset array to avoid unnecessary switch calls */

	if ( spectrum_toggle && !party_toggle )
	{
		spectrum_pwm();
	}

	if ( !spectrum_toggle && party_toggle )
	{
		party_pwm();
	}
#if 0
    if ( pwm_handler.new_redvalue )
    {
        pwm_handler.new_redvalue = 0;
        __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, pwm_handler.red_value);
        HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
    }

    if ( pwm_handler.new_greenvalue )
    {
        pwm_handler.new_greenvalue = 0;
        __HAL_TIM_SET_COMPARE(&htim14, TIM_CHANNEL_1, pwm_handler.green_value);
        HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
    }

    if ( pwm_handler.new_bluevalue )
    {
        pwm_handler.new_bluevalue = 0;
        __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, pwm_handler.blue_value);
        HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
    }

    if ( pwm_handler.new_whitevalue )
    {
        pwm_handler.new_whitevalue = 0;
        __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, pwm_handler.white_value);
        HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
    }
#endif
}

static void red_pwm(void)
{
    pwm_handler.red_value = (uint16_t)r_msg_nextion[1] << 8;
    pwm_handler.red_value |= r_msg_nextion[0];
    __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, pwm_handler.red_value);
    HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
}

static void green_pwm(void)
{
    pwm_handler.green_value = (uint16_t)r_msg_nextion[1] << 8;
    pwm_handler.green_value |= r_msg_nextion[0];
    __HAL_TIM_SET_COMPARE(&htim14, TIM_CHANNEL_1, pwm_handler.green_value);
    HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
}

static void blue_pwm(void)
{
    pwm_handler.blue_value = (uint16_t)r_msg_nextion[1] << 8;
    pwm_handler.blue_value |= r_msg_nextion[0];
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, pwm_handler.blue_value);
    HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
}

static void white_pwm(void)
{
    pwm_handler.white_value = (uint16_t)r_msg_nextion[1] << 8;
    pwm_handler.white_value |= r_msg_nextion[0];
    __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, pwm_handler.white_value);
    HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
}

static void spectrum_pwm(void)
{
	static uint8_t i = 0;
	static uint32_t ticks_now = 0;
	static uint32_t ticks_past = 0;

	ticks_now = HAL_GetTick();

	/* This method is compliant only for dimmable RGB led strip that Red channels is used to dim */
	if ( ticks_now > (ticks_past + 10) )
	{
		switch(i)
		{
			case 0:
				pwm_handler.green_value += 1;
				
				if ( pwm_handler.green_value == 1000 )
					++i;
				break;
				
			case 1:
				pwm_handler.blue_value += 1;
				
				if ( pwm_handler.blue_value == 1000 )
					++i;
				break;
				
			case 2:
				pwm_handler.green_value -= 1;
				pwm_handler.blue_value -= 1;
				
				if ( (pwm_handler.green_value == 0) && (pwm_handler.blue_value == 0) )
					i = 0;
				break;
				
			default:
				break;
		}
	}
	
	/* Set new PWM values to outputs */
	if ( pwm_handler.red_value == 0 )
	{
		pwm_handler.red_value = 1000;
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, pwm_handler.red_value);
	}
	
	__HAL_TIM_SET_COMPARE(&htim14, TIM_CHANNEL_1, pwm_handler.green_value); 
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, pwm_handler.blue_value);
}

static void party_pwm(void)
{

}
/* The end of file */
