/*----------------------------------------------------
 * Name:        leds.c
 * Purpose:     
 *
 *----------------------------------------------------
 * Project:     X project
 *----------------------------------------------------
 * Compiler:    
 *----------------------------------------------------
 * Copyright:
 *----------------------------------------------------
 * Created:     27.07.2018 by savojuus
 *----------------------------------------------------
 * Globals:
 *----------------------------------------------------
 * $Rev: 0000 $:
 * $Author: xxxxxx $:
 * $Date: 2000-01-01 00:00:00 +0300 (xx, 01 tammi 2000) $:
 *----------------------------------------------------
 */


/*
 * Include files
 */
#include "leds.h"
#include "gpio.h"
#include "filter.h"

/*
 * Local macro definitions
 */
 
 
/*
 * Type definitions
 */
extern ENCODER_T encoder;

/*
 * Local variables
 */
uint16_t blinks[4] = {BLINK100, BLINK250, BLINK500, BLINK1000};
 
/* 
 * Function prototypes 
 */
 
 
/* 
 * Functions
 */
void init_leds(void)
{
    HAL_GPIO_WritePin(RED_GPIO_Port, RED_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GREEN_GPIO_Port, GREEN_Pin, GPIO_PIN_SET);
    encoder.enc_button = 3;
}

void handle_leds(void)
{
    static uint32_t green_ticks = 0;
    uint32_t now = HAL_GetTick();

    if ( ( now - green_ticks ) >= blinks[encoder.enc_button] )
    {
        green_ticks = now;
        HAL_GPIO_TogglePin(GREEN_GPIO_Port, GREEN_Pin);
    }
}

/* The end of file */
