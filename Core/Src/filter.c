/*----------------------------------------------------
 * Name:        filter.c
 * Purpose:     
 *
 *----------------------------------------------------
 * Project:     X project
 *----------------------------------------------------
 * Compiler:    
 *----------------------------------------------------
 * Copyright:
 *----------------------------------------------------
 * Created:     27.07.2018 by savojuus
 *----------------------------------------------------
 * Globals:
 *----------------------------------------------------
 * $Rev: 0000 $:
 * $Author: xxxxxx $:
 * $Date: 2000-01-01 00:00:00 +0300 (xx, 01 tammi 2000) $:
 *----------------------------------------------------
 */


/*
 * Include files
 */
#include "main.h"
#include "filter.h"
#include "gpio.h"

/*
 * Local macro definitions
 */
 
 
/*
 * Type definitions
 */
ENCODER_T encoder = {0};
FILTER_T pulse_filter[NBR_OF_PULSES_IN] = {0};
FILTER_T button_filter = {0};

/*
 * Local variables
 */

 
/* 
 * Function prototypes 
 */
 static void check_button(int8_t result);
 static void check_pulses(int8_t result, uint8_t io);

/* 
 * Functions
 */
void init_filter(void)
{
    uint8_t i;

    for ( i = 0; i < NBR_OF_PULSES_IN; i++)
    {
        pulse_filter[i].up_time = UP_TIME_PULSE;
        pulse_filter[i].down_time = DOWN_TIME_PULSE;
        pulse_filter[i].state = 0;
        pulse_filter[i].last_state = 0;
    }

    button_filter.up_time = UP_TIME_BUTTON;
    button_filter.down_time = DOWN_TIME_BUTTON;
    button_filter.state = 1;
    button_filter.last_state = 1;
}

void filter_io(void)
{
    static uint8_t button_val = 1;
//    static uint8_t a_val = 1;
//    static uint8_t b_val = 1;

    button_val = HAL_GPIO_ReadPin(ENCODER_BUTTON_GPIO_Port, ENCODER_BUTTON_Pin);
    check_button(button_val);

//    a_val = HAL_GPIO_ReadPin(ENCODER_A_GPIO_Port, ENCODER_A_Pin);
//    check_pulses(a_val, ENCODER_A);
//
//    b_val = HAL_GPIO_ReadPin(ENCODER_B_GPIO_Port, ENCODER_B_Pin);
//    check_pulses(b_val, ENCODER_B);
}

/* TODO: Encoder handling, interrupt needed for implementing OR
 * use already existing HAL function
 */
static void check_pulses(int8_t result, uint8_t io)
{
#if 0
    if ( result == 0 && pulse_filter[0].ticks == 0 && io == ENCODER_A )
    {
        pulse_filter[0].ticks = HAL_GetTick();
    }

    if ( result == 0 && pulse_filter[1].ticks == 0 && io == ENCODER_B )
    {
        pulse_filter[1].ticks = HAL_GetTick();
    }

    if ( pulse_filter[0].ticks != 0 && pulse_filter[1].ticks != 0 )
    {
        if ( pulse_filter[0].ticks < pulse_filter[1].ticks )
        {
            /* Clockwise */
            pulse_filter[0].ticks = 0;
            encoder.encoderA++;
        }
        else
        {
            /* Counter-clockwise*/
            pulse_filter[1].ticks = 0;
            encoder.encoderB++;
        }

        if ( encoder.encoderA == 20 )
        {
            encoder.encoderA = 0;
            HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
        }
    }
#endif
}

static void check_button(int8_t result)
{
    if ( result == 0 && button_filter.ticks == 0)
    {
        button_filter.ticks = HAL_GetTick();
        button_filter.state = TRANSITION_LOW;
    }
    else if ( result == 1 )
    {
        button_filter.ticks = 0;
        button_filter.state = TRANSITION_HIGH;
    }
    else
    {
        button_filter.last_ticks = HAL_GetTick();

        if ( button_filter.state == TRANSITION_LOW && ( (button_filter.last_ticks - button_filter.ticks) > button_filter.down_time ) )
        {
            HAL_GPIO_TogglePin(RED_GPIO_Port, RED_Pin);
            button_filter.state = NO_TRANSITION;

            encoder.enc_button += 1;

            if ( encoder.enc_button > 3 )
            {
                encoder.enc_button = 0;
            }
        }
        else
        {
            /* Button is kept pressed or nothing is happening */
        }
    }
}

/* The end of file */
