/*----------------------------------------------------
 * Name:        init.c
 * Purpose:     
 *
 *----------------------------------------------------
 * Project:     X project
 *----------------------------------------------------
 * Compiler:    
 *----------------------------------------------------
 * Copyright:
 *----------------------------------------------------
 * Created:     27.07.2018 by savojuus
 *----------------------------------------------------
 * Globals:
 *----------------------------------------------------
 * $Rev: 0000 $:
 * $Author: xxxxxx $:
 * $Date: 2000-01-01 00:00:00 +0300 (xx, 01 tammi 2000) $:
 *----------------------------------------------------
 */


/*
 * Include files
 */
#include "init.h"
#include "pwm.h"
#include "leds.h"
#include "filter.h"
#include "usart.h"

/*
 * Local macro definitions
 */
 
 
/*
 * Type definitions
 */


/*
 * Local variables
 */
extern UART_HandleTypeDef huart1;
 
/* 
 * Function prototypes 
 */
 
 
/* 
 * Functions
 */
void initialize_all(void)
{
    init_leds();
    init_pwm();
    init_filter();
//    start_uart();
}

/* The end of file */
